using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARManager : MonoBehaviour
{
    public GameObject prefabSnake;
    public GameObject prefabAR_Snake;

    public AnimalManager animalManager;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitPrefab()
    {

        Destroy(animalManager.animalSpawned);

        animalManager.SpawnAnimal("Serpent");
        //GameObject snake = Instantiate(prefabSnake);
        animalManager.animalSpawned.transform.parent = prefabAR_Snake.transform;

        prefabAR_Snake.SetActive(true);
    }
}
