using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SpriteTextureManager : MonoBehaviour
{

    int numberImg = 0;


    public Texture2D InitTexture(Texture2D capTexture, Texture2D maskTexture)
    {
        Texture2D customTex = new Texture2D(256, 256);
        Color32[] pixelsCap = capTexture.GetPixels32();
        Color32[] pixelsMask = maskTexture.GetPixels32();
        Color32[] newPixels = new Color32[pixelsMask.Length];

        for (int i = 0; i < pixelsMask.Length; i++)
        {
            if(pixelsMask[i].r == 0 && pixelsMask[i].g == 0 && pixelsMask[i].b == 0)
            {
                newPixels[i] = pixelsMask[i];
            }
            else
            {
                newPixels[i] = pixelsCap[i];
            }
        }

        customTex.SetPixels32(newPixels);
        customTex.Apply();

        //SaveToPNG(customTex);
        return customTex;
    }






    Texture2D toTexture2D(RenderTexture rTex)
    {
        Texture2D tex = new Texture2D(256, 256);
        // ReadPixels looks at the active RenderTexture.
        RenderTexture.active = rTex;
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0, 0);
        tex.Apply();
        return tex;
    }


    public Color32 AverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;

        float r = 0;
        float g = 0;
        float b = 0;

        int numberCount = total;

        for (int i = 0; i < total; i++)
        {
            if (texColors[i].r == 0 && texColors[i].g == 0 && texColors[i].b == 0)
            {
                numberCount -= 1;
            }
            else
            {
                r += texColors[i].r;
                g += texColors[i].g;
                b += texColors[i].b;
            }
        }

        return new Color32((byte)(r / numberCount), (byte)(g / numberCount), (byte)(b / numberCount), 0);
    }

    void SaveToPNG(Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();

        var dirPath = Application.dataPath + "/../SaveImages/";
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }
        File.WriteAllBytes(dirPath + "Image_" + numberImg + ".png", bytes);
        numberImg += 1;
    }
}

