using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Capture_Draw : MonoBehaviour
{

    public Mask mask;
    public Camera cameraVu;

    public int FileCounter = 0;

    public AnimalManager animalManager = new AnimalManager();

    public ARManager aRManager = new ARManager();


    public void getPictureFromMask()
    {
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = cameraVu.targetTexture;

        cameraVu.Render();
        
        Texture2D Image = new Texture2D(cameraVu.targetTexture.width, cameraVu.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, cameraVu.targetTexture.width, cameraVu.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        animalManager.drawCapture = Image;
        aRManager.InitPrefab();
        //var Bytes = Image.EncodeToPNG();
        //Destroy(Image);

        //File.WriteAllBytes(Application.dataPath + "/" + FileCounter + ".png", Bytes);
        //FileCounter++;


    }

    public RenderTexture GetCameraRT(Camera cam)
    {
        var rt = RenderTexture.GetTemporary(Screen.width, Screen.height);
        cam.targetTexture = rt;
        cam.Render();
        cam.targetTexture = null;
        return rt;
    }

}
