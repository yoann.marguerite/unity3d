using UnityEngine;
using System.Net;
using System.IO;
using System;
using System.Linq;
using TMPro;

public class ScriptPartager : MonoBehaviour
{
    public TMP_InputField nameInput;
    public TMP_InputField emailInput;
    public TMP_InputField hashtagInput;
    public HubspotContactResult hubspotContactResult = new HubspotContactResult();
    public string errorMessage;
    public TextMeshProUGUI errorMessageField;
    public string succesfullMessage;
    public TextMeshProUGUI succesfullMessageField;
    public string reseau;
    // Start is called before the first frame update
    void Start() { }

    // Update is called once per frame
    void Update() { }

    public void SetReseau(string reseau)
    {
        this.reseau = reseau;
    }

    public void SendData()
    {
        SendProspect(nameInput.text, emailInput.text, hashtagInput.text);
    }

    public void ShowError(string message)
    {

        errorMessage = message;
        Debug.Log(errorMessage);
        if (errorMessageField != null)
        {
            errorMessageField.text = errorMessage;
        }
    }

    public void SendProspect(string name, string email, string hashtag)
    {
        ShowError("");
        if (name == "")
        {
            ShowError("Veuillez introduire votre prénom.");
        }
        else if (email == "")
        {
            ShowError("Veuillez introduire votre email.");
        }
        else if (hashtag == "")
        {
            ShowError("Veuillez introduire votre hashtag.");
        }
        else if (reseau == "")
        {
            ShowError("Veuillez choisir un réseau sur le quel partager votre capture.");
        }
        else
        {
            HubspotContactResult.Contact contact = GetContact(email);
            UpsertProspect(contact, name, email, hashtag);
        }
    }

    private void UpsertProspect(HubspotContactResult.Contact contact, string name, string email, string hashtag)
    {

        var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://api.hubapi.com/crm/v3/objects/contacts?hapikey=eu1-68d3-b23d-4f94-8abc-dcd66a742179");
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        if (contact != null)
        {
            httpWebRequest = (HttpWebRequest)WebRequest.Create($"https://api.hubapi.com/crm/v3/objects/contacts/{contact.id}?hapikey=eu1-68d3-b23d-4f94-8abc-dcd66a742179");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PATCH";
        }

        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
            if (contact == null)
            {
                string json = "{" +
                             "\"properties\":{" +
                               "\"firstname\":\"" + name + "\"," +
                               "\"email\":\"" + email + "\"," +
                               "\"message_hashtag\":\"" + hashtag + "\"," +
                               "\"date_hashtag\":\"" + DateTime.Now.ToString("dd-MM-yyyy") + "\"," +
                               "\"reseau_hashtag\":\"" + reseau + "\"" +
                             "}" +
                            "}";
                streamWriter.Write(json);
            }
            else
            {
                string json = "{" +
                             "\"properties\":{" +
                               "\"firstname\":\"" + name + "\"," +
                               "\"message_hashtag\":\"" + contact.properties.message_hashtag + ", " + hashtag + "\"," +
                               "\"date_hashtag\":\"" + contact.properties.date_hashtag + ", " + DateTime.Now.ToString("dd-MM-yyyy") + "\"," +
                               "\"reseau_hashtag\":\"" + contact.properties.reseau_hashtag + ", " + reseau + "\"" +
                             "}" +
                            "}";
                streamWriter.Write(json);
            }

        }
        try
        {
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                succesfullMessage = "Votre capture a bien été partagée !";
                Debug.Log("Succes");
                if (succesfullMessageField != null)
                {
                    succesfullMessageField.text = succesfullMessage;
                }

            }
        }
        catch (WebException e)
        {
            Debug.Log(e.Message);
            ShowError("Veuillez introduire un email valide.");
        }
    }

    public HubspotContactResult.Contact GetContact(string email)
    {
        var url = "https://api.hubapi.com/crm/v3/objects/contacts?hapikey=eu1-68d3-b23d-4f94-8abc-dcd66a742179&properties=email,firstname,message_hashtag,date_hashtag,reseau_hashtag";

        var httpRequest = (HttpWebRequest)WebRequest.Create(url);

        httpRequest.Accept = "application/json";

        var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        {
            var result = streamReader.ReadToEnd();
            hubspotContactResult = JsonUtility.FromJson<HubspotContactResult>(result);
            HubspotContactResult.Contact contact = hubspotContactResult.results.SingleOrDefault(contact => contact.properties.email == email);
            return contact;
        }
    }

}

