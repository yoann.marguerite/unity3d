﻿using System;
using System.Collections.Generic;

[Serializable]
public class HubspotContactResult
{
    [UnityEngine.SerializeField]
    public List<Contact> results = new List<Contact>();

    [Serializable]
    public class Contact
    {
        public string id;
        [UnityEngine.SerializeField]
        public Properties properties = new Properties();

        [Serializable]
        public class Properties
        {
            public string email;
            public string firstname;
            public string message_hashtag;
            public string date_hashtag;
            public string reseau_hashtag;
        }

    }

}
