using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;

public class VuforiaCamera : MonoBehaviour
{

    //public GameObject CameraUser;
    public Camera CameraCaptur;
    public RenderTexture renderOverlay;

    private IEnumerator DelayedInitCapture()
{
        CameraCaptur.enabled = false;
        CameraCaptur.targetTexture = renderOverlay;
        yield return new WaitForSeconds(0.5f); // Wait time to let init finish. If black screen still persists increase wait time
        CameraCaptur.enabled = true;
    }

    public void RestartCapture()
    {
        StartCoroutine(DelayedInitCapture());
    }

    public void RestartCamUser()
    {
        StartCoroutine(DelayedInitUser());
    }
    private IEnumerator DelayedInitUser()
    {
        CameraCaptur.enabled = false;
        CameraCaptur.targetTexture = null;
        yield return new WaitForSeconds(0.5f); // Wait time to let init finish. If black screen still persists increase wait time
        CameraCaptur.enabled = true;
    }

    public GameObject CanvasActived;

    public void TakeCapture()
    {
        StartCoroutine(CaptureIt());
    }

    IEnumerator CaptureIt()
    {
        CanvasActived.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        string fileName = "Screenshot_AR_EPSI.png";
        string pathToSave = Application.persistentDataPath + "/" + fileName;
        ScreenCapture.CaptureScreenshot(pathToSave);
        yield return new WaitForEndOfFrame();
        CanvasActived.SetActive(true);
    }

    private void Start()
    {
    }

    void SaveToPNG(Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();

        var dirPath = Application.dataPath + "/../SaveImages/";
        if (!Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }
        File.WriteAllBytes(dirPath + "Image_Cap.png", bytes);
    }

    public Texture2D GetCapture()
    {
        Texture2D texture = null;
        string[] files = Directory.GetFiles(Application.persistentDataPath + "/", "*.png");
        foreach(string file in files)
        {
            Debug.Log(file);
            if (file == Application.persistentDataPath +"/" + "Screenshot_AR_EPSI.png")
            {
                byte[] fileBytes;
                if (File.Exists(file))
                {
                    fileBytes = File.ReadAllBytes(file);
                    texture = new Texture2D(2, 2, TextureFormat.RGBA32, false);
                    texture.LoadImage(fileBytes);
                }
            }
        }

        return texture;
    }
}
