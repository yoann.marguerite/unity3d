using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalManager : MonoBehaviour
{

    public Texture2D drawCapture;

    [SerializeField]
    public List<Animal> animals = new List<Animal>();

    public SpriteTextureManager spriteTextureManager;

    public GameObject animalSpawned;


    public void Awake()
    {
        if (spriteTextureManager == null)
        {
            spriteTextureManager = gameObject.GetComponent<SpriteTextureManager>();
        }
    }

    public void Start()
    {
        //SpawnAnimal("Serpent");
    }

    public void SpawnAnimal(string aName)
    {
        foreach (Animal animal in animals)
        {
            if (animal.aName == aName)
            {
                InitColorBodyAnimal(animal);
                InitMaterialPrefab(animal);
            }
        }
    }

    public void InitColorBodyAnimal(Animal animal)
    {
        foreach (BodyAnimal body in animal.bodyAnimals)
        {
            Texture2D texture = spriteTextureManager.InitTexture(drawCapture, body.maskBody);

            Color32 colorAverage = spriteTextureManager.AverageColorFromTexture(texture);
            
            body.color = colorAverage;
        }
    }

    public void InitMaterialPrefab(Animal animal)
    {

        if (animal.prefabScript == null)
        {
            animalSpawned = Instantiate(animal.prefab3D);
            animal.prefabScript = animalSpawned.GetComponent<AnimalPref>();
        }

        foreach (BodyAnimal body in animal.bodyAnimals)
        {
            animal.prefabScript.ChangeColorMesh(body.color, body.name_mat);
        }
    }

    public void InitAnimal()
    {

    }
}
